<!--
SPDX-FileCopyrightText: 2021 Florian Warzecha <florian_filip.warzecha@fh-bielefeld.de>

SPDX-License-Identifier: MPL-2.0

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
-->

Simple tool to display the results of certain scheduling algorithms in table form.

# License

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.

Note that certain configuration and data files are licensed under CC0-1.0.
