// SPDX-FileCopyrightText: 2021 Florian Warzecha <florian_filip.warzecha@fh-bielefeld.de>
//
// SPDX-License-Identifier: MPL-2.0
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use structscan::UserSubmittable;

use crate::table::Table;

#[derive(Debug, UserSubmittable, derive_new::new)]
pub struct Process {
    pub name: String,
    pub duration: usize,
    pub priority: usize,
}

impl Process {
    pub fn table(processes: &[Process]) -> Table {
        let mut table = Table::new();

        table.set(0, 0, "Prozess");
        table.set(1, 0, "Dauer");
        table.set(2, 0, "Priorität");

        for (index, process) in processes.iter().enumerate() {
            table.set(0, index + 1, &process.name);
            table.set(1, index + 1, &process.duration);
            table.set(2, index + 1, &process.priority);
        }

        table
    }
}
