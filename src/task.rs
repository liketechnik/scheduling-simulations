// SPDX-FileCopyrightText: 2021 Florian Warzecha <florian_filip.warzecha@fh-bielefeld.de>
//
// SPDX-License-Identifier: MPL-2.0
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use structscan::UserSubmittable;

use crate::table::Table;

#[derive(Debug, UserSubmittable, derive_new::new)]
pub struct Task {
    pub name: String,
    pub duration: usize,
    pub deadline: usize,
}

impl Task {
    pub fn table(tasks: &[Task]) -> Table {
        let mut table = Table::new();

        table.set(0, 0, "Task");
        table.set(1, 0, "Dauer");
        table.set(2, 0, "Deadline");

        for (index, task) in tasks.iter().enumerate() {
            table.set(0, index + 1, &task.name);
            table.set(1, index + 1, &task.duration);
            table.set(2, index + 1, &task.deadline);
        }

        table
    }
}

#[derive(Debug, UserSubmittable, derive_new::new)]
pub struct ArrivingTask {
    pub name: String,
    pub duration: usize,
    pub deadline: usize,
    pub arrival: usize,
}

impl ArrivingTask {
    pub fn table(tasks: &[ArrivingTask]) -> Table {
        let mut table = Table::new();

        table.set(0, 0, "Task");
        table.set(1, 0, "Ankunft");
        table.set(2, 0, "Dauer");
        table.set(3, 0, "Deadline");

        for (index, task) in tasks.iter().enumerate() {
            table.set(0, index + 1, &task.name);
            table.set(1, index + 1, &task.arrival);
            table.set(2, index + 1, &task.duration);
            table.set(3, index + 1, &task.deadline);
        }

        table
    }
}
