// SPDX-FileCopyrightText: 2021 Florian Warzecha <florian_filip.warzecha@fh-bielefeld.de>
//
// SPDX-License-Identifier: MPL-2.0
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::convert::TryInto;

use crate::{
    table::Table,
    task::{ArrivingTask, Task},
};

pub trait TaskProperties {
    fn name(&self) -> &str;
    fn duration(&self) -> usize;
}

macro_rules! task_properties_impl {
    ($struct:ty) => {
        impl TaskProperties for $struct {
            fn name(&self) -> &str {
                &self.name
            }
            fn duration(&self) -> usize {
                self.duration
            }
        }
    };
}

task_properties_impl!(Task);
task_properties_impl!(ArrivingTask);

pub fn setup_table<T: TaskProperties>(tasks: Vec<&T>) -> Table {
    let mut table = Table::new();

    table.set(0, 0, "Zeit");
    for index in 0..(tasks.iter().fold(0, |acc, t| acc + t.duration())) {
        table.set(0, index + 1, index);
    }

    for (index, task) in tasks.iter().enumerate() {
        table.set(index + 1, 0, &task.name());
    }

    table
}

pub fn earliest_due_date(tasks: Vec<Task>) -> Table {
    let mut table = setup_table(tasks.iter().collect());

    let mut tasks: Vec<(usize, Task)> = tasks.into_iter().enumerate().collect();

    let mut column = 1;
    while tasks.iter().any(|(_, t)| t.duration > 0) {
        let (index, (row, task)) = tasks
            .iter_mut()
            .enumerate()
            .filter(|(_, (_, t))| t.duration > 0)
            .min_by_key(|(_, (_, t))| t.deadline)
            .unwrap();
        task.duration -= 1;

        let mut content = String::from("x");
        if task.duration == 0 {
            content.push('e');
        }
        // we're interested if the deadline
        // lies in the beginning of the next interval
        // -1: column to actual time
        // +1: we want to know the deadline 1 step *before* it's happening
        if column >= 2 && task.deadline == column - 1 + 1 {
            content.push('d');
        }
        table.set(*row + 1, column, content);

        let row = *row;
        for (row, task) in tasks.iter().filter(|(r, _)| *r != row) {
            if column >= 2 && task.deadline == column - 1 + 1 {
                table.set(*row + 1, column, "d");
            }
        }

        column += 1;

        // make sure the current task get's reselected, if multiple tasks could be selected
        let task = tasks.remove(index);
        tasks.insert(0, task);
    }

    table
}

pub fn earliest_deadline_first(tasks: Vec<ArrivingTask>) -> Table {
    let mut table = setup_table(tasks.iter().collect());

    let mut tasks: Vec<(usize, ArrivingTask)> = tasks.into_iter().enumerate().collect(); // we change the order of tasks below, so assing static indices here...

    let mut time = 0;
    while tasks.iter().any(|(_, t)| t.duration > 0) {
        let (index, (row, task)) = match tasks
            .iter_mut()
            .enumerate()
            .filter(|(_, (_, t))| t.duration > 0 && t.arrival <= time)
            .min_by_key(|(_, (_, t))| t.deadline)
        {
            None => {
                time += 1;
                continue;
            }
            Some(v) => v,
        };

        task.duration -= 1;

        let mut content = String::from("x");
        if task.duration == 0 {
            content.push('e');
        }
        if task.arrival == time {
            content.insert(0, 'a');
        }
        if time > 0 && task.deadline == time + 1 {
            content.push('d');
        }

        table.set(*row + 1, time + 1, content);

        let row = *row;
        for (row, task) in tasks.iter().filter(|(r, _)| *r != row) {
            let mut content = String::new();
            if task.arrival == time {
                content.push('a');
            }
            if time > 0 && task.deadline == time + 1 {
                content.push('d');
            }
            table.set(*row + 1, time + 1, content);
        }

        time += 1;

        // make sure the current task get's reselected, if multiple tasks could be selected
        let task = tasks.remove(index);
        tasks.insert(0, task);
    }

    table
}

pub fn least_laxity(tasks: Vec<ArrivingTask>) -> Table {
    let mut table = setup_table(tasks.iter().collect());

    let mut tasks: Vec<(usize, ArrivingTask)> = tasks.into_iter().enumerate().collect(); // we change the order of tasks below, so assing static indices here...

    let mut time = 0;
    while tasks.iter().any(|(_, t)| t.duration > 0) {
        let (slack, (index, (row, task))) = match tasks
            .iter_mut()
            .enumerate()
            .filter(|(_, (_, t))| t.duration > 0 && t.arrival <= time)
            .map(|(index, (row, task))| {
                let deadline: i64 = task
                    .deadline
                    .try_into()
                    .expect("Simulation too big for i64");
                let time: i64 = time.try_into().expect("Simulation too big for i64");
                let duration: i64 = task
                    .duration
                    .try_into()
                    .expect("Simulation too big for i64");
                let slack: i64 = deadline - time - duration;

                (slack, (index, (row, task)))
            })
            .min_by_key(|(slack, (_, (_, _)))| *slack)
        {
            None => {
                time += 1;
                continue;
            }
            Some(v) => v,
        };

        task.duration -= 1;

        let mut content = "x".to_string();
        if task.duration == 0 {
            content.push('e');
        }
        if task.arrival == time {
            content.insert(0, 'a');
        }
        if time > 0 && task.deadline == time + 1 {
            content.push('d');
        }
        content.push_str(&format!("{}", slack));

        table.set(*row + 1, time + 1, content);

        let row = *row;
        for (row, task) in tasks.iter().filter(|(r, _)| *r != row) {
            let mut content = String::new();
            if task.arrival == time {
                content.push('a');
            }
            if time > 0 && task.deadline == time + 1 {
                content.push('d');
            }
            table.set(*row + 1, time + 1, content);
        }

        time += 1;

        // make sure the current task get's reselected, if multiple tasks could be selected
        let task = tasks.remove(index);
        tasks.insert(0, task);
    }

    table
}
