// SPDX-FileCopyrightText: 2021 Florian Warzecha <florian_filip.warzecha@fh-bielefeld.de>
//
// SPDX-License-Identifier: MPL-2.0
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use crate::{process::Process, table::Table};

use std::cmp::PartialOrd;
use std::convert::TryFrom;
use std::fmt::Write;

pub fn first_come_first_served(processes: Vec<Process>) -> Table {
    print!("Reihenfolge: ");
    let mut first = true;
    for process in &processes {
        if !first {
            print!(", ");
        }
        print!("{}", process.name);
        first = false;
    }
    println!();

    let term: Vec<(usize, usize)> = processes
        .iter()
        .enumerate()
        .map(|(index, p)| (processes.len() - index, p.duration))
        .collect();
    let result: f32 = term
        .iter()
        .fold(0.0f32, |total_duration, (p_left, duration)| {
            f32::from(u16::try_from(p_left * duration).unwrap()) + total_duration
        })
        / f32::from(u16::try_from(processes.len()).unwrap());

    print!("T = (");
    let mut first = true;
    for component in &term {
        if !first {
            print!(" + ");
        }
        print!("{} * {}", component.0, component.1);
        first = false;
    }
    println!(") / {} = {}", processes.len(), result);

    Table::new()
}

pub fn shortest_job_first(mut processes: Vec<Process>) -> Table {
    processes.sort_by(|p1, p2| p1.duration.cmp(&p2.duration));
    first_come_first_served(processes) // fcfs just takes the order it gets :D
}

pub fn priority_based(mut processes: Vec<Process>) -> Table {
    processes.sort_by(|p1, p2| p1.priority.cmp(&p2.priority).reverse());
    first_come_first_served(processes) // fcfs just takes the order it gets :D
}

pub fn round_robin(mut processes: Vec<Process>) -> Table {
    let time_index = processes.len();

    let mut table = Table::new();

    table.set(0, time_index, "Zeit");
    table.set(1, time_index, "0");
    for (index, process) in processes.iter().enumerate() {
        table.set(0, index, process.name.clone());
        table.set(1, index, process.duration);
    }

    let mut row = 2;
    let mut times = Vec::with_capacity(processes.len());
    let mut last_time = 0;
    while processes.iter().any(|p| p.duration > 0) {
        let process = processes
            .iter()
            .filter(|p| p.duration > 0)
            .min_by_key(|p| p.duration)
            .unwrap();

        let p_name = process.name.clone();
        let p_duration = process.duration;
        let p_count = processes.iter().filter(|p| p.duration > 0).count();
        let current_duration = last_time + p_count * p_duration;

        table.set(
            row,
            time_index,
            format!(
                "T_V{}={} + {} * {} = {}",
                process.name, last_time, p_count, p_duration, current_duration
            ),
        );
        for (index, process) in processes.iter_mut().enumerate() {
            if process.duration > 0 {
                process.duration -= p_duration;
            }

            if p_name == process.name {
                table.set(
                    row - 1,
                    index,
                    format!("*{}*", process.duration + p_duration),
                );
            }

            table.set(row, index, process.duration);
        }

        times.push(current_duration);
        last_time = current_duration;
        row += 1;
    }

    print!("T = (");
    let mut first = true;
    for t in &times {
        if !first {
            print!(" + ");
        }
        print!("{}", t);
        first = false;
    }
    println!(
        ") / {} = {}",
        processes.len(),
        f32::from(u16::try_from(times.iter().sum::<usize>()).unwrap())
            / f32::from(u16::try_from(processes.len()).unwrap())
    );

    table
}

pub fn round_robin_honoring_priority(mut processes: Vec<Process>) -> Table {
    let slice_index = processes.len();
    let execution_time_index = slice_index + 1;
    let process_time_index = execution_time_index + 1;

    let mut table = Table::new();
    table.set(0, slice_index, "Anteile gesamt:");
    table.set(0, execution_time_index, "Anteile benötigt:");
    for (index, process) in processes.iter().enumerate() {
        table.set(0, index, format!("{}({})", process.name, process.priority));
        table.set(1, index, process.duration);
    }

    let mut row = 2;
    let mut times = Vec::with_capacity(processes.len());
    let mut last_time = 0;
    while processes.iter().any(|p| p.duration > 0) {
        let process = processes
            .iter()
            .filter(|p| p.duration > 0)
            .max_by(|p1, p2| {
                (f32::from(u16::try_from(p1.priority).unwrap())
                    / f32::from(u16::try_from(p1.duration).unwrap()))
                .partial_cmp(
                    &(f32::from(u16::try_from(p2.priority).unwrap())
                        / f32::from(u16::try_from(p2.duration).unwrap())),
                )
                .unwrap()
            })
            .unwrap();

        let iterations = process.duration / process.priority;

        let slices: usize = processes
            .iter()
            .filter(|p| p.duration > 0)
            .map(|p| p.priority)
            .sum();

        let duration = last_time + iterations * slices;

        let mut slices_calculation = String::new();
        let mut first = true;
        for slice in processes.iter().filter(|p| p.duration > 0) {
            if !first {
                write!(&mut slices_calculation, "+").unwrap();
            }
            write!(&mut slices_calculation, "{}", slice.priority).unwrap();
            first = false;
        }
        write!(&mut slices_calculation, "={}", slices).unwrap();
        table.set(row - 1, slice_index, slices_calculation);

        table.set(
            row - 1,
            execution_time_index,
            format!("{}*{}", iterations, slices),
        );

        let mut finished_processes = String::new();
        for (index, process) in processes.iter_mut().enumerate() {
            if process.duration > 0 {
                process.duration -= process.priority * iterations;

                if process.duration == 0 {
                    write!(&mut finished_processes, "T_{}=", process.name).unwrap();
                    times.push(duration);
                }
            }

            table.set(row, index, process.duration);
        }
        write!(
            &mut finished_processes,
            "={}+{}*{}={}",
            last_time, iterations, slices, duration
        )
        .unwrap();
        table.set(row - 1, process_time_index, finished_processes);

        last_time = duration;
        row += 1;
    }

    print!("T = (");
    let mut first = true;
    for t in &times {
        if !first {
            print!(" + ");
        }
        print!("{}", t);
        first = false;
    }
    println!(
        ") / {} = {}",
        processes.len(),
        f32::from(u16::try_from(times.iter().sum::<usize>()).unwrap())
            / f32::from(u16::try_from(processes.len()).unwrap())
    );

    table
}
