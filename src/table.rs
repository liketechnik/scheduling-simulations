// SPDX-FileCopyrightText: 2021 Florian Warzecha <florian_filip.warzecha@fh-bielefeld.de>
//
// SPDX-License-Identifier: MPL-2.0
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::fmt::Display;

use crate::config::{COLUMN_SEPARATOR, ROW_SEPARATOR};

type Cell = Option<String>;

type Row = Vec<Cell>;

pub struct Table {
    content: Vec<Row>,
    column_lengths: Vec<Option<usize>>,
    max_row_length: usize,
}

impl Table {
    pub fn new() -> Table {
        Table {
            content: Vec::new(),
            column_lengths: Vec::new(),
            max_row_length: 0,
        }
    }

    pub fn set<Content: Display>(&mut self, row: usize, column: usize, c: Content) {
        let content = c.to_string();

        if column + 1 > self.max_row_length {
            self.max_row_length = column + 1;
        }

        if column + 1 > self.column_lengths.len() {
            self.column_lengths.resize_with(column + 1, || Option::None);
        }
        let column_len = self.column_lengths.get_mut(column).unwrap();
        match column_len {
            None => *column_len = Some(content.len()),
            Some(max_len) => {
                let current_len = content.len();
                if current_len > *max_len {
                    *column_len = Some(current_len);
                }
            }
        }

        if row + 1 > self.content.len() {
            self.content.resize_with(row + 1, Vec::new);
        }
        let row = self.content.get_mut(row).unwrap();

        if column + 1 > row.len() {
            row.resize_with(column + 1, || Option::None);
        }
        row[column] = Some(content);
    }

    fn row_separator(&self) {
        print!("{}", ROW_SEPARATOR);
        for column_len in &self.column_lengths {
            for _ in 0..(column_len.unwrap_or(1) + 2) {
                print!("{}", ROW_SEPARATOR);
            }

            print!("{}", ROW_SEPARATOR);
        }
    }

    pub fn print(&self) {
        if self.content.is_empty() {
            return;
        }

        self.row_separator();
        println!();

        for row in &self.content {
            print!("{}", COLUMN_SEPARATOR);

            for (index, cell) in row.iter().enumerate() {
                print!(
                    "{:^width$}",
                    cell.as_ref().map_or("", |s| s),
                    width = self.column_lengths[index].unwrap_or(1) + 2
                );
                print!("{}", COLUMN_SEPARATOR);
            }

            let remainder = self.max_row_length - row.len();
            for index in (row.len())..(row.len() + remainder) {
                print!(
                    "{:^width$}",
                    "",
                    width = self.column_lengths[index].unwrap_or(1) + 2
                );
                print!("{}", COLUMN_SEPARATOR);
            }
            println!();

            self.row_separator();
            println!();
        }
    }
}

impl Default for Table {
    fn default() -> Self {
        Self::new()
    }
}
