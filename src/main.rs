// SPDX-FileCopyrightText: 2021 Florian Warzecha <florian_filip.warzecha@fh-bielefeld.de>
//
// SPDX-License-Identifier: MPL-2.0
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use structscan::{FromUser, UserSubmittable};
use strum::{self, EnumMessage, IntoEnumIterator};

use std::{io, marker::PhantomData, str::FromStr, string::ToString};

use scheduling_simulations::{
    process::Process,
    schedulers::{embedded, operating_systems},
    table::Table,
    task::{ArrivingTask, Task},
};

#[derive(strum::ToString, strum::EnumString, strum::EnumIter, strum::EnumMessage)]
#[strum(ascii_case_insensitive)]
enum SchedulingSimulation {
    #[strum(serialize = "ES", to_string = "Embedded Systems")]
    Embedded,
    #[strum(serialize = "OS", to_string = "Operating Systems")]
    Operating,
}

impl ChoiceName for SchedulingSimulation {
    fn choice_name() -> &'static str {
        "style of scheduling simulation"
    }
}

trait Algorithm {}

#[derive(strum::ToString, strum::EnumString, strum::EnumIter, strum::EnumMessage, Debug)]
#[strum(ascii_case_insensitive)]
enum EsAlgorithm {
    #[strum(serialize = "EDD", to_string = "Earliest Due Date")]
    Edd,
    #[strum(serialize = "EDF", to_string = "Earliest Deadline First")]
    Edf,
    #[strum(serialize = "LL", to_string = "Least Laxity")]
    Ll,
}

impl Algorithm for EsAlgorithm {}

impl<A> ChoiceName for A
where
    A: Algorithm,
{
    fn choice_name() -> &'static str {
        "algorithm"
    }
}

#[derive(strum::ToString, strum::EnumString, strum::EnumIter, strum::EnumMessage, Debug)]
#[strum(ascii_case_insensitive)]
enum OsAlgorithm {
    #[strum(serialize = "FCFS", to_string = "First-Come-First-Served")]
    Fcfs,
    #[strum(serialize = "SJF", to_string = "Shortest Job First")]
    Sjf,
    #[strum(serialize = "PB", to_string = "Priority Based")]
    Pb,
    #[strum(serialize = "RR", to_string = "Round Robin")]
    Rr,
    #[strum(serialize = "RRP", to_string = "Round Robin honoring Priorities")]
    Rrp,
}

impl Algorithm for OsAlgorithm {}

struct SchedulingFunction<F, U, T, I>
where
    F: Fn(Vec<U>) -> Table,
    U: UserSubmittable<T, I>,
    T: FromUser<I>,
{
    function: F,
    _phantom: PhantomData<T>,
    _phantom2: PhantomData<I>,
    _phantom3: PhantomData<U>,
}

impl<F, U, T, I> SchedulingFunction<F, U, T, I>
where
    F: Fn(Vec<U>) -> Table,
    U: UserSubmittable<T, I>,
    T: FromUser<I>,
{
    pub fn new(fun: F) -> Self {
        SchedulingFunction {
            function: fun,
            _phantom: PhantomData,
            _phantom2: PhantomData,
            _phantom3: PhantomData,
        }
    }

    pub fn collect_input(&self, user_input_container: &mut String) -> Vec<U> {
        let mut items = Vec::new();

        loop {
            let user_input;
            // Make sure the lock on stdin is released before we use it again below...
            {
                let stdin = std::io::stdin();
                let mut stdin = stdin.lock();
                user_input = <U>::from_user(&mut stdin);
            }
            if let Some(user_input) = user_input {
                items.push(user_input);
            }

            println!("Add another task? (y)/n");
            io::stdin()
                .read_line(user_input_container)
                .expect("Failed to read from stdin");
            if !(user_input_container.trim().is_empty()
                || user_input_container.trim() == "y"
                || user_input_container.trim() == "Y"
                || user_input_container.trim() == "yes")
            {
                user_input_container.clear();
                break;
            }
            user_input_container.clear();
        }

        items
    }

    pub fn execute(&self, task: Vec<U>) -> Table {
        (self.function)(task)
    }
}

fn main() {
    loop {
        // ================
        // Simulation style
        // ================

        let mut user_input = String::new();
        let simulation_style = match SchedulingSimulation::choose(&mut user_input) {
            Some(choice) => choice,
            None => continue,
        };
        user_input.clear();

        // =========
        // Algorithm
        // =========
        match simulation_style {
            SchedulingSimulation::Embedded => {
                let algorithm = match EsAlgorithm::choose(&mut user_input) {
                    Some(choice) => choice,
                    None => continue,
                };
                user_input.clear();

                match algorithm {
                    EsAlgorithm::Edd => {
                        let fun = SchedulingFunction::new(embedded::earliest_due_date);
                        let inputs = fun.collect_input(&mut user_input);
                        Task::table(&inputs).print();
                        fun.execute(inputs).print();
                    }
                    EsAlgorithm::Edf => {
                        let fun = SchedulingFunction::new(embedded::earliest_deadline_first);
                        let inputs = fun.collect_input(&mut user_input);
                        ArrivingTask::table(&inputs).print();
                        fun.execute(inputs).print();
                    }
                    EsAlgorithm::Ll => {
                        let fun = SchedulingFunction::new(embedded::least_laxity);
                        let inputs = fun.collect_input(&mut user_input);
                        ArrivingTask::table(&inputs).print();
                        fun.execute(inputs).print();
                    }
                };
            }
            SchedulingSimulation::Operating => {
                let algorithm = match OsAlgorithm::choose(&mut user_input) {
                    Some(choice) => choice,
                    None => continue,
                };
                user_input.clear();

                match algorithm {
                    OsAlgorithm::Fcfs => {
                        let fun =
                            SchedulingFunction::new(operating_systems::first_come_first_served);
                        let inputs = fun.collect_input(&mut user_input);
                        Process::table(&inputs).print();
                        fun.execute(inputs);
                    }
                    OsAlgorithm::Sjf => {
                        let fun = SchedulingFunction::new(operating_systems::shortest_job_first);
                        let inputs = fun.collect_input(&mut user_input);
                        Process::table(&inputs).print();
                        fun.execute(inputs);
                    }
                    OsAlgorithm::Pb => {
                        let fun = SchedulingFunction::new(operating_systems::priority_based);
                        let inputs = fun.collect_input(&mut user_input);
                        Process::table(&inputs).print();
                        fun.execute(inputs);
                    }
                    OsAlgorithm::Rr => {
                        let fun = SchedulingFunction::new(operating_systems::round_robin);
                        let inputs = fun.collect_input(&mut user_input);
                        Process::table(&inputs).print();
                        fun.execute(inputs).print();
                    }
                    OsAlgorithm::Rrp => {
                        let fun = SchedulingFunction::new(
                            operating_systems::round_robin_honoring_priority,
                        );
                        let inputs = fun.collect_input(&mut user_input);
                        Process::table(&inputs).print();
                        fun.execute(inputs).print();
                    }
                };
            }
        }
    }
}

trait VariantChooser<E> {
    fn choose(user_input: &mut String) -> Option<E>;
}

trait ChoiceName {
    fn choice_name() -> &'static str;
}

impl<E> VariantChooser<E> for E
where
    E: IntoEnumIterator + ToString + EnumMessage + FromStr + ChoiceName,
{
    fn choose(user_input: &mut String) -> Option<E> {
        println!("Please specify the {} to use:", <E>::choice_name());

        for choice in <E>::iter() {
            println!(
                "* {} ({})",
                choice.to_string(),
                choice.get_serializations()[0]
            );
        }

        io::stdin()
            .read_line(user_input)
            .expect("Failed to read from stdin");

        match <E>::from_str(user_input.trim()) {
            Ok(choice) => Some(choice),
            Err(_) => {
                println!("Invalid choice");
                None
            }
        }
    }
}
